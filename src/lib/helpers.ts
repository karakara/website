/**
 * @returns `true` if value is nullish 
 */
//#__INLINE__
export function isNullish(value: any): value is null|undefined {
  return !(value != null);
}

/**
 * @returns `false` if `value` is nullish
 */
//#__INLINE__
export function isNonNullish(value: any): value is {} {
  return value != null;
}

/**
 * Does nothing. Useful as a placeholder callback.
 */
export function noop(): void {}

/**
 * @param milliseconds The number of milliseconds to wait.
 * @returns A promise that resolves after the specified number of milliseconds.
 */
export function timeout(milliseconds: number): Promise<void> {
  return new Promise((resolve) => setTimeout(resolve, milliseconds));
}

/**
 * @returns A zero-padded string.
 * @example
 * ```js
 * console.log(zeroPad(12, 3)) // logs "012"
 * ```
 */
export function zeroPad(value: number|string, digits: number): string {
  if (typeof value === "number") return zeroPad(value.toString(), digits);
  return value.length < digits ? zeroPad(0 + value, digits) : value;
}
