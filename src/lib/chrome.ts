
import { isNonNullish } from "./helpers";

const document = globalThis.document ?? {};
const navigator = globalThis.navigator ?? {};
const userAgent = navigator.userAgent ?? "";
const window = globalThis.window ?? {};

let geckoMatch;

export const isChromiumBased =
  !(
    (
      userAgent.includes("AppleWebKit") &&
      !userAgent.includes("Chrome")
    ) ||
    (
      geckoMatch = (userAgent.match(/(?:like )?Gecko/) ?? [])[0],
      isNonNullish(geckoMatch)
        ? !geckoMatch.includes("like")
        : false
    )
  ) && (
    "userAgentData" in navigator ||
    "chrome" in window
  );

export const hasBrowsingTopics =
  "browsingTopics" in document;

export const isBrave =
  isChromiumBased &&
  isNonNullish((navigator as any).brave) &&
  typeof (navigator as any).brave.isBrave === "function";

export const isGoogleChrome =
  isChromiumBased &&
  (navigator as any).userAgentData?.brands?.some((data: { brand: string }) => data.brand === "Google Chrome");
