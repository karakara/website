
import { isNonNullish, zeroPad } from "$lib/helpers";

/**
 * 
 */
export function deobfuscate(obfuscated: string): string {
  let deobfuscated = "";

  let hexKey = obfuscated.substring(0, 2);
  let key = parseInt(hexKey, 16);
  
  if (isNaN(key)) {
    throw new Error("Invalid obfuscated string");
  }

  for (let i = 2; i < obfuscated.length; i += 2) {
    let hexChar = obfuscated.substring(i, i + 2);
    let char = parseInt(hexChar, 16);

    if (isNaN(char)) {
      throw new Error("Invalid obfuscated string");
    }
    
    deobfuscated += String.fromCharCode(char ^ key);
  }

  return deobfuscated;
}

/**
 * @returns `true` if value might be an obfuscated string.
 */
export function isObfuscated(value: string): boolean {
  return isNonNullish(value.match(/^[0-9a-f]+$/gi));
}

export function obfuscate(deobfuscated: string, key: number = (Math.random() * 255) >> 0): string {
  let obfuscated = "";
  
  obfuscated += zeroPad((key << 0).toString(16), 2);

  for (let i = 0; i < deobfuscated.length; i++) {
    let charCode = deobfuscated.charCodeAt(i);

    obfuscated += zeroPad((charCode ^ key).toString(16), 2);
  }

  return obfuscated;
}
