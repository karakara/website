# Website

This repository contains the source code for my website.

It's built using SvelteKit, via the static adapter and hosted using nginx.

The website is live at https://insertdomain.name.

## Build for Yourself

Running for yourself should™ be as simple as running the following:

```sh
pnpm install
pnpm run twemoji # fetches twemoji, needs curl, imagemagick, and unzip installed
pnpm run build
```
## Disclaimer

This isn't open source so that other people can use it. It's open source so that
if something about my site bothers someone, they can create a merge request to
fix whatever it is that bothers them.
