# Derived from plus.st's robots.txt
User-agent: *
    User-Agent: Adsbot
    User-Agent: AdsBot-Google
    User-Agent: AdsBot-Google-Mobile
    Disallow: /
    User-Agent: TurnitinBot
    Disallow: /
    User-Agent: NPBot
    Disallow: /
    User-Agent: SlySearch
    Disallow: /
    User-Agent: BLEXBot
    Disallow: /
    User-agent: CheckMarkNetwork/1.0 (+https://www.checkmarknetwork.com/spider.html)
    Disallow: /
    User-agent: BrandVerity/1.0
    Disallow: /
    User-agent: ChatGPT-User
    User-agent: GPTBot
    User-agent: CCBot
    User-agent: CCBot/2.0
    User-agent: CCBot/3.1
    Disallow: /