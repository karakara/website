#!/bin/sh
# Usage: deobfuscate OBFUSCATED
# IE: deobfuscate 4a272f253d
deobfuscate() {
  obfuscated="$1";
  deobfuscated="";

  hex_key="$(printf "%s" "$obfuscated" | cut -c 1-2)";
  key=$(printf "%d" "0x$hex_key" 2>/dev/null);

  if [ ${?} -ne 0 ]; then
    echo "Invalid obfuscated string";
    return 1;
  fi

  obfuscated_length=$(printf "%s" "$obfuscated" | wc -c);
  obfuscated_length=$((${obfuscated_length} - 1));

  i=3;
  while [ ${i} -le ${obfuscated_length} ]; do
    hex_char="$(printf "%s" "$obfuscated" | cut -c "${i}-$((${i}+1))")";
    char=$(printf "%d" "0x${hex_char}" 2>/dev/null);

    if [ ${?} -ne 0 ]; then
      echo "Invalid obfuscated string";
      return 1;
    fi

    deobfuscated="$(printf "%s\\x$(printf %x $((char ^ key)))" "${deobfuscated}")";
    i=$((i + 2));
  done

  echo "${deobfuscated}";
}
