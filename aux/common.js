
import process from "node:process";
import path from "node:path";
import fs from "node:fs/promises";
import { fileURLToPath } from "node:url";

/**
 * Ensures that the process is in the correct pwd.
 */
export function ensureCorrectPwd() {
  const scriptPath = fileURLToPath(import.meta.url);
  const rootPath = path.dirname(path.dirname(scriptPath));
  process.chdir(rootPath);
}

/**
 * Removes all the files in `directory`, except for those specified in `exceptions`.
 * 
 * @param {string} directory
 * @param {string[]} exceptions
 * @returns {Promise<void>}
 */
export async function removeAllExcept(directory, exceptions) {
  const files = new Set(await fs.readdir(directory));

  for (const exception of exceptions) files.delete(exception);

  const tasks = new Set();

  for (const file of files) {
    tasks.add((async () => {
      await fs.rm(path.join(directory, file), { force: true, recursive: true });
    })());
  }

  await Promise.all(tasks);
}

/**
 * Removes all the files in `directory` that match the specified `pattern`.
 * 
 * @param {string} directory 
 * @param {RegExp} pattern
 * @returns {Promise<void>}
 */
export async function removeAllMatching(directory, pattern) {
  const files = await fs.readdir(directory);

  const matches = new Set();

  for (const file of files) if (pattern.test(file)) matches.add(file);
  
  const tasks = new Set();

  for (const match of matches) {
    tasks.add((async () => {
      await fs.rm(path.join(directory, match), { force: true, recursive: true });
    })());
  }

  await Promise.all(tasks);
}
