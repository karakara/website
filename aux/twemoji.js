
import { spawn } from "node:child_process";
import path from "node:path";
import fs from "node:fs/promises";

import { ensureCorrectPwd, removeAllExcept, removeAllMatching } from "./common.js";

////////////////////////////////////////////////////////////////////////////////

const REPOSITORY = "https://github.com/jdecked/twemoji";

////////////////////////////////////////////////////////////////////////////////

/**
 * Converts an image file from one format to another.
 * 
 * @param {string} input
 * @param {string} output
 * @returns {Promise<void>}
 */
function convert(input, output) {
  return new Promise((resolve, reject) => {
    const child = spawn("convert", [ input, output ]);

    let stdout = "";
    child.stdout.on("data", (chunk) => stdout += chunk);
    
    let stderr = "";
    child.stderr.on("data", (chunk) => stderr += chunk);
    
    child.on("error", reject);
    
    child.on("exit", (code) => {
      if (code !== 0) {
        reject(new Error(`Convert exited with a non-zero exit code.\nStdout:\n${stdout}\nStderr:\n${stderr}`));
      } else {
        resolve();
      }
    });
  });
}

/**
 * Unzips a zip archive as specified by `archive` into the folder specified by
 * `destination`.
 * 
 * @param {string} archive 
 * @param {string} destination 
 * @returns {Promise<void>}
 */
function unzip(archive, destination) {
  return new Promise((resolve, reject) => {
    const child = spawn("unzip", [ archive, "-d", destination ]);

    let stdout = "";
    child.stdout.on("data", (chunk) => stdout += chunk);
    
    let stderr = "";
    child.stderr.on("data", (chunk) => stderr += chunk);
    
    child.on("error", reject);
    
    child.on("exit", (code) => {
      if (code !== 0) {
        reject(new Error(`Unzip exited with a non-zero exit code.\nStdout:\n${stdout}\nStderr:\n${stderr}`));
      } else {
        resolve();
      }
    });
  });
}

////////////////////////////////////////////////////////////////////////////////

// Ensure the correct working directory
ensureCorrectPwd();

// Cleanup past invocations
console.log("Cleaning up...");
await Promise.all([
  removeAllExcept("static/assets/twemoji", [ ".gitignore" ]),
  removeAllMatching("/tmp", /twemoji-\d+(\.\d+)+/g),
  fs.rm("/tmp/twemoji.zip", { force: true }),
]);

// Get the latest release
console.log("Finding the latest release...");
const latestRelease = await (async () => {
  const request = await fetch(`${REPOSITORY}/releases/latest`, { redirect: "follow" });
  const response = await request.text();
  return /(?<=<title>Release Twemoji )\d+(\.\d+)+/gm.exec(response)[0];
})();

// Download and extract the latest release
await (async () => {
  console.log("Downloading the latest release...");
  const request = await fetch(`${REPOSITORY}/archive/refs/tags/v${latestRelease}.zip`, { redirect: "follow" });
  await fs.writeFile("/tmp/twemoji.zip", request.body);
  console.log("Extracting the latest release...");
  await unzip("/tmp/twemoji.zip", "/tmp");
  await fs.rm("/tmp/twemoji.zip");
})();

// Copy the relevant files
console.log("Copying assets...");
await fs.cp(`/tmp/twemoji-${latestRelease}/assets`, "static/assets/twemoji", { recursive: true });

// Convert .png to .webm
console.log("Converting png to webp...");
const tasks = new Set();
const files = await fs.readdir("static/assets/twemoji/72x72");
for (const file of files) {
  tasks.add(convert(
    path.join("static/assets/twemoji/72x72", file),
    path.join("static/assets/twemoji/72x72", file.replace(/.png$/, ".webp"))))
}
await Promise.all(tasks);

console.log("Cleaning up...");
await removeAllMatching("/tmp", /twemoji-\d+(\.\d+)+/g);
