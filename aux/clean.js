
import fs from "node:fs/promises";

import { ensureCorrectPwd, removeAllExcept } from "./common.js";

ensureCorrectPwd();

await Promise.all([
  fs.rm(".svelte-kit", { force: true, recursive: true }),
  fs.rm("build", { force: true, recursive: true }),
  fs.rm("node_modules", { force: true, recursive: true }),
  removeAllExcept("static/assets/twemoji", [ ".gitignore" ]),
]);
